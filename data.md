<div align="center">
  <br>
  <a href="https://gitlab.com/GitLabBooks"><img src="img/Banner.png" alt="GitBookBanner" width="500">
  </a>
</div>
<div align="center">
    <a href="https://discord.gg/6f5WgDBj5W"><img alt="Discord" src="https://img.shields.io/badge/Discord-white?style=social&logo=discord"></a>
    <a href="https://pinterest.com/GitBookArch"><img alt="Pinterest" src="https://img.shields.io/badge/Pinterest-white?style=social&logo=pinterest"></a>
    <a href="https://facebook.com/GitBookArch"><img alt="Discord" src="https://img.shields.io/badge/Facebook-white?style=social&logo=facebook"></a>
    <a href="https://gitlab.com/GitLabBooks"><img alt="GitLab" src="https://img.shields.io/badge/GitLab-white?style=social&logo=gitlab"></a>
    <a href="https://github.com/GitBookArch"><img alt="GitHub" src="https://img.shields.io/badge/GitHub-white?style=social&logo=github"></a>
  <br>
  <br>
</div>  

Dentro de la carpeta `BooksTemplate` encontraremos los siguientes archivos:
```bash
BooksTemplate
│   readme.md
│   covers.py    
└───books
│   │   Python.pdf
└───data
|   │   data.xlsx
|   │   repo.json
└───scripts
    │   poppler.zip
```

# 🗎 **`readme.md`**
El archivo `readme.md` contiene la descripción del repositorio.
```md
`Editorial` `Colección`
```
Donde:
+ `Editorial` -> San Marcos, Lumbreras, Rodo, ...
+ `Colección` -> Temas Selectos, Compendios, ...
    
# 🗀 **`books`**    
El carpeta `books` almacenará todos nuestros archivos pdf.
> Eliminar el archivo `Python.pdf`

# 🗎 **`data.xlsx`**  
El archivo excel `data.xlsx` contiene la siguiente tabla.

| id | titulo              | nombre            | editorial   | curso          | tags      |
|----|---------------------|-------------------|-------------|----------------|-----------|
| 1  | compendio de física | compendiodefísica | san marcos  | física         | compendio |
| 2  | título del libro 2  | nombre2           | editorial 2 | curso2         | tag2      |
| 3  | título del libro 3  | nombre2           | editorial 3 | curso2, curso3 | tag2,tag3 |

Donde:
+ `id` -> 1, 2, 3, 4, ...
+ `titulo` -> Título del libro.
+ `nombre` -> Nombre del archivo pdf.
    > Puede nombrarlo como mejor le parezca, pero se recomienda el mismo nombre que el título.
+ `curso` ->   Curso al que pertenece el libro.
    > Si el libro contiene más de un curso, dichos cursos deben estar separardos por comas(`física,química,biología`).
+ `tags` -> Los tags(etiquetas) añaden información a la descripción del libro. Por ejemplo, si tenemos un texto de física (Tema selecto estática nivel básico). Las etiquetas a rescatar serian(`tema selecto,estática,básico`).
    > Si el libro contiene más de una etiqueta, separarlos por comas(`tag1,tag2,tag3`).
        
    ⚠️ **Advertencia**
    > No modificar los nombres de las columnas(`id`, `titulo`, `nombre`, `curso`, `tags`).


    > Todos los datos deben de llenarse en `minúsculas`.


    > El `nombre` del archivo pdf no debe contener caracteres vacíos(❌`compendio de fisica`) en lugar de eso podria nombrarlos como(✔️ `CompendioDeFísica` | ✔️ `compendiodefísica`). Solo en este campo puede omitir la regla anterior(sólo uso de minúsculas).

# 🗎 **`repo.json`**   
El archivo `repo.json` contiene la ruta y el nombre de usuario del colaborado.
```json
{
    "path_repo": "GitLabBooks/BooksTemplate",
    "made_by": "GitBookArch"
}
```
Donde:
+ `path_repo` -> Parte de la url `https://gitlab.com/GitLabBooks/BooksTemplate`.
+ `made_by` -> Username del colaborador.