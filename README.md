<div align="center">
  <br>
  <a href="https://gitlab.com/GitLabBooks"><img src="img/Banner.png" alt="GitBookBanner" width="500">
  </a>
</div>
<div align="center">
    <a href="https://discord.gg/6f5WgDBj5W"><img alt="Discord" src="https://img.shields.io/badge/Discord-white?style=social&logo=discord"></a>
    <a href="https://pinterest.com/GitBookArch"><img alt="Pinterest" src="https://img.shields.io/badge/Pinterest-white?style=social&logo=pinterest"></a>
    <a href="https://facebook.com/GitBookArch"><img alt="Discord" src="https://img.shields.io/badge/Facebook-white?style=social&logo=facebook"></a>
    <a href="https://gitlab.com/GitLabBooks"><img alt="GitLab" src="https://img.shields.io/badge/GitLab-white?style=social&logo=gitlab"></a>
    <a href="https://github.com/GitBookArch"><img alt="GitHub" src="https://img.shields.io/badge/GitHub-white?style=social&logo=github"></a>
  <br>
  <br>
</div>

# **¿Te gustaría contribuir?**
### **Prerrequisitos**
--- 
1. Tener una cuenta en `GitLab` y si en caso no tuviera, puede crearlo gratuitamente en [GitLab](https://gitlab.com/users/sign_up).

2. Descargar e instalar `python` y `git` desde :
 - [`git`](https://git-scm.com/download/win)
 - [`python 3.8`](https://www.python.org/ftp/python/3.8.7/python-3.8.7-amd64.exe)

### **Configuración**
---
1. Git
    + Abrir una `terminal/consola/cmd` y ejecutar los siguientes comandos:
      ```bash
      git config --global user.name "username"
      ```
      ```bash
      git config --global user.email"email@example.com"
      ```
   > `username` y `email@example.com` son los datos con las que registraste en  [GitLab](https://gitlab.com/users/sign_up).
2. Python
    + Abrir una `terminal/consola/cmd` y ejecutar los siguientes comandos:
      ```bash
      pip install pdf2image unidecode
      ```

> **Nota:** Para abrir una `terminal/consola/cmd` dar `click derecho` dentro del escritorio o la carpeta de trabajo. Automaticamente se mostrar una paleta de opciones de donde tendremos que seleccionar  `Git Bash Here`.

### **¿Cómo subo mis archivos pdf?**
---
1. Crear carpeta del proyecto
   > Por ejemplo la podemos crear dentro del directorio de `Documentos` y con el nombre `GitBookArch`

2. Acceder al carpeta creada en el paso anterior `GitBookArch` 
	+ Dentro de la carpeta `GitBookArch` abrir una `terminal` y ejecutar `git clone url`, por ejemplo:
	    ```bash
        git clone https://gitlab.com/GitLabBooks/BooksTemplate.git
        ```
3. Si en el paso anterior no dio ningún error, notará que dentro de la carpeta `GitBookArch` se creó la carpeta `BooksTemplate` 
	+ Dentro de la carpeta `BooksTemplate` encontraremos los siguientes archivos:
        ```bash
        BooksTemplate
        │   readme.md
        │   covers.py    
        └───books
        │   │   Python.pdf
        └───data
        |   │   data.xlsx
        |   │   repo.json
        └───scripts
            │   poppler.zip
        ```
    > Antes de continuar con el paso 4. [[**Leer**]](data.md)

4. En este paso crearemos las portadas de los libros, para ello abriremos una terminal dentro de la carpeta `BooksTemplate` y ejecutaremos el siguiente comando.
    ```bash
    python covers.py
    ```
    El script `covers.py` creará en automático la carpeta `covers`.

5. Finalmente subiremos los documentos al repositio remoto. Abrir una terminal dentro de la carpeta `BooksTemplate` y ejecutar los siguiente comandos:
    ```bash
    git add .
    ```
    ```bash
    git commit -m "update"
    ```
    ```bash
    git push
    ```